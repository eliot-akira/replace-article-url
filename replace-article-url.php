<?php
/*
Plugin Name: Replace Article URLs
Description: One-off plugin to search & replace go_link field, to prepend http protocol if it doesn't have one.
Version: 0.0.4
Require PHP: 5.6
*/

class ReplaceArticleURLs {

  private $page_name = 'replace-article-urls';

  function __construct() {

    add_action('admin_menu', function() {
      add_options_page('Replace Article URLs', 'Replace Article URLs', 'manage_options', $this->page_name, [$this, 'settings_page']);
    });
  }

  function settings_page() {

    $base_url = admin_url('options-general.php?page='.$this->page_name);
    $url_dry_run = add_query_arg([ 'run' => 'dry' ], $base_url);
    $url_run = add_query_arg([ 'run' => 'real' ], $base_url);

    $result = isset($_GET['run']) ? $this->run($_GET['run']) : '';

?>
<div class="wrap">

  <h2>Replace Article URLs</h2>

  <br>
  <a href="<?= $url_dry_run ?>"><button type="button" class="button">Dry Run</button></a> &nbsp;
  <a href="<?= $url_run ?>"><button type="button" class="button button-primary">For Real</button></a>

  <br><br>

  <?= $result ?>

</div>
<?php

}

  function run($run_mode = 'dry') {

    $meta_key = 'go_link';

    $posts = get_posts([
      'meta_key' => $meta_key,
      'meta_value' => '',
      'meta_compare' => 'EXISTS',
      'post_type' => 'any',
      'post_status' => 'any',
      'posts_per_page' => -1
    ]);

    $total = count($posts);
    $updated = 0;
    $is_for_real = $run_mode==='real';

    foreach ($posts as $post) {

      $value = get_post_meta($post->ID, 'go_link', true);

      if (!empty($value) && is_string($value)
        && $value[0] !== '<' // Can be <script> or <iframe>
        && !$this->starts_with_http($value)
      ) {
        if ($is_for_real) {
          update_post_meta($post->ID, 'go_link', "http://$value");
        }
        $updated++;
      }
    }

    ob_start();

?>
<p>Found <?= $total.' post'.($total===1 ? '' : 's') ?> with article URL.</p>
<p><?= $updated.' post'.($updated===1 ? '' : 's') ?> <?=
  $is_for_real ? '' : 'should be '
?>updated.</p>
<?php

    return ob_get_clean();
  }

  function starts_with_http($value = '') {
    return substr($value, 0, 4) === 'http';
  }
}

new ReplaceArticleURLs;
